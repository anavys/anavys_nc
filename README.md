# Nextcloud with php fpm, postgres, and auto-proxy by jwilder.

Dokumentation of nextcloud Repo: 
- https://github.com/nextcloud/docker

Stack is originating from the examples in there:  
- https://github.com/nextcloud/docker/tree/master/.examples/docker-compose/with-nginx-proxy/postgres/fpm

## Problems

- https://github.com/nextcloud/docker/issues/345